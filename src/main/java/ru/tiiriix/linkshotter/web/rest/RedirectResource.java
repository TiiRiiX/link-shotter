package ru.tiiriix.linkshotter.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.tiiriix.linkshotter.service.ShotterService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/l")
public class RedirectResource {

    @Autowired
    private ShotterService shotterService;

    @RequestMapping(value = "/{shortLink}", method = RequestMethod.GET)
    public void redirect(@PathVariable String shortLink, HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location", shotterService.getLongLink(shortLink));
        httpServletResponse.setStatus(302);
    }

}
