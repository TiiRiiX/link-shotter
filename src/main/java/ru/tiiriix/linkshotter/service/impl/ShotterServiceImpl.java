package ru.tiiriix.linkshotter.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tiiriix.linkshotter.domain.Link;
import ru.tiiriix.linkshotter.service.LinkService;
import ru.tiiriix.linkshotter.service.ShotterService;
import ru.tiiriix.linkshotter.web.rest.dto.OriginalLinkDto;
import ru.tiiriix.linkshotter.web.rest.dto.ShortLinkDto;

import java.util.Random;

@Service
public class ShotterServiceImpl implements ShotterService {

    @Autowired
    private LinkService linkService;

    private static final int LENGTH_SHORT_LINK = 6;
    private static final String ALL_SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private String makeShortText(String longText) {
        Random rand = new Random(longText.hashCode());
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < LENGTH_SHORT_LINK; i++) {
            stringBuilder.append(ALL_SYMBOLS.charAt(rand.nextInt(ALL_SYMBOLS.length())));
        }
        return stringBuilder.toString();
    }

    @Override
    public ShortLinkDto makeShortLink(OriginalLinkDto originalLink) {
        String longLink = originalLink.getOriginal();
        Link existLink = linkService.findLinkByLongLink(longLink);
        if (existLink == null) {
            ShortLinkDto shortLink = new ShortLinkDto(makeShortText(longLink));
            Link link = new Link(shortLink.getLink(), longLink);
            linkService.saveLink(link);
            return shortLink;
        } else {
            return existLink.getShortLinkDto();
        }
    }

    @Override
    public String getLongLink(String shortLink) {
        Link link = linkService.findLinkByShortLink(shortLink);
        if (link == null) {
            return null;
        } else {
            return link.getLongLink();
        }
    }
}
