package ru.tiiriix.linkshotter.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tiiriix.linkshotter.domain.Link;
import ru.tiiriix.linkshotter.repositories.LinkRepository;
import ru.tiiriix.linkshotter.service.LinkService;

import java.util.ArrayList;

@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    private LinkRepository linkRepository;

    private Link getFirstFromIterable(Iterable<Link> iterable) {
        ArrayList<Link> linksList = new ArrayList<>();
        iterable.forEach(linksList::add);
        if (linksList.size() > 0) {
            return linksList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Link findLinkByShortLink(String shortLink) {
        Iterable<Link> links = linkRepository.findLinksByShortLink(shortLink);
        return getFirstFromIterable(links);
    }

    @Override
    public boolean checkLinkExistByShortLink(String shortLink) {
        Link link = findLinkByShortLink(shortLink);
        return link != null;
    }

    @Override
    public Link findLinkByLongLink(String longLink) {
        Iterable<Link> links = linkRepository.findLinksByLongLink(longLink);
        return getFirstFromIterable(links);
    }

    @Override
    public boolean checkLinkExistByLongLink(String longLink) {
        Link link = findLinkByLongLink(longLink);
        return link != null;
    }

    @Override
    public Link saveLink(Link link) {
        return linkRepository.save(link);
    }

}
