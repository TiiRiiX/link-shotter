package ru.tiiriix.linkshotter.service;

import ru.tiiriix.linkshotter.web.rest.dto.OriginalLinkDto;
import ru.tiiriix.linkshotter.web.rest.dto.ShortLinkDto;

public interface ShotterService {
    ShortLinkDto makeShortLink(OriginalLinkDto originalLink);

    String getLongLink(String shortLink);
}
