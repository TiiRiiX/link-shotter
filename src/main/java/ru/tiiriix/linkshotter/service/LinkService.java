package ru.tiiriix.linkshotter.service;

import ru.tiiriix.linkshotter.domain.Link;

public interface LinkService {

    Link findLinkByShortLink(String shortLink);

    boolean checkLinkExistByShortLink(String shortLink);

    Link findLinkByLongLink(String longLink);

    boolean checkLinkExistByLongLink(String longLink);

    Link saveLink(Link link);

}
